export class Sorting {
  public type: SortingType;
  public direction: SortingDirection;
}

export enum SortingType {
  ByName = 1,
  ByYear = 2
}

export enum SortingDirection {
  Asc = 1,
  Desc = 2
}
