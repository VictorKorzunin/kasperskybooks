/**
 * Validation error DTO.
 */
export interface ValidationError {
  message: string;
  modelState: { [key: string]: string[] };
}
