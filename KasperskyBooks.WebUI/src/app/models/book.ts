export class Book {
  id: number;
  title: string;
  authors: Author[] = [];
  pageCount: number;
  issuer: string;
  issueYear: number;
  isbn: string;
  imageUrl: string;
}

export class Author {
  firstName: string;
  lastName: string;
}
