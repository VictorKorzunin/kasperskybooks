import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';

import { Book } from '../../models/book';
import { DialogResult } from '../../models/dialog-result';
import { AppState, coreActions } from '../../reducers';
import { BookService } from '../../services/book.service';
import { DialogService } from '../../services/dialog.service';
import { BookEditDialogComponent } from '../book-edit/book-edit-dialog.component';

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BookItemComponent {

  constructor(
    private store: Store<AppState>,
    private bookService: BookService,
    private dialogService: DialogService,
  ) { }

  @Input()
  public value = new Book();

  public prepareAuthorsText(value: Book) {
    return value.authors.map(r => `${r.firstName} ${r.lastName}`).join(', ');
  }

  public async editClick(value: Book) {
    const result = await this.dialogService.showEditDialog(BookEditDialogComponent, value);
    if (result) {
      this.store.dispatch(new coreActions.UpdateBook(result));
    }
  }

  public async deleteClick() {
    const result = await this.dialogService.confirm('Are you sure?', 'Delete book');
    if (result === DialogResult.YES) {
      await this.bookService.delete(this.value.id);
      this.store.dispatch(new coreActions.DeleteBook(this.value.id));
    }
  }

}
