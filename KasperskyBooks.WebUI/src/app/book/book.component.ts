import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { Book } from '../models/book';
import { Sorting, SortingDirection, SortingType } from '../models/sorting';
import { AppState, coreActions, syncActions } from '../reducers';
import { BookService } from '../services/book.service';
import { deepClone } from '../helpers/util';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  constructor(
    private bookService: BookService,
    private store: Store<AppState>,
  ) { }

  public books$: Observable<Book[]> = Observable.combineLatest(this.store.select(s => s.core.books), this.store.select(s => s.sync.sorting))
    .map(([books, sorting]) => {
      if (sorting && sorting.type) {
        const clonedBooks = deepClone(books);
        return clonedBooks.sort((a, b) => {
          switch (sorting.type) {
            case SortingType.ByName: return (a.title > b.title) !== (sorting.direction === SortingDirection.Asc) ? -1 : 1;
            case SortingType.ByYear: return (a.issueYear > b.issueYear) !== (sorting.direction === SortingDirection.Asc) ? -1 : 1;
            default: return 0;
          }
        });
      } else {
        return books;
      }
    });

  public sorting: Sorting;

  public ngOnInit() {
    this.store.select(s => s.sync.sorting).subscribe(s => this.sorting = s);
    this.store.dispatch(new coreActions.FetchBooks());
  }

  public changeSorting(type: SortingType, direction: SortingDirection) {
    this.store.dispatch(new syncActions.SetSorting({type, direction}));
  }

}
