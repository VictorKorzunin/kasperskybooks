import { AfterViewChecked, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { NgForm, AbstractControl } from '@angular/forms';

import { Author, Book } from '../../models/book';
import { BookService } from '../../services/book.service';
import { ValidationError } from '../../models/validation-error';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.scss']
})
export class BookEditComponent implements AfterViewChecked {

  constructor(private bookService: BookService) {}

  @Input()
  public book = new Book();

  @ViewChild(NgForm)
  public form: NgForm;

  @Output()
  public submit = new EventEmitter<Book>();

  public currentYear = new Date().getFullYear();

  public progress: number;

  /**
   * Error emitter to pass it to form.
   */
  public errorEmitter = new EventEmitter<ValidationError>();

  public ngAfterViewChecked(): void {
    if (!this.book.authors.length) {
      this.addAuthor();
    }
  }

  public addAuthor() {
    this.book.authors.push(new Author());
  }

  public deleteAuthor(author: Author) {
    this.book.authors.splice(this.book.authors.indexOf(author), 1);
  }

  private getErrors(c: AbstractControl) {
    return c && c.errors ? Object.keys(c.errors).filter(w => Number.isInteger(+w)).map(k => c.errors[k]) : [];
  }

  public async onSubmit(form: NgForm) {
    if (form.valid) {
      try {
        if (!this.book.id) {
          const bookId = await this.bookService.create(this.book);
          this.book.id = bookId;
        } else {
          await this.bookService.update(this.book);
        }
        this.submit.next(this.book);
      } catch (error) {
        if (error instanceof HttpErrorResponse && error.status === 400) {
          this.errorEmitter.emit(error.error);
        } else {
          throw error;
        }
      }
    }
  }

  public async browserImageClick(event: Event) {
    this.progress = 0;
    const input = event.target as HTMLInputElement;
    this.form.controls['imageUrl'].markAsTouched();
    try {
      const result = await this.bookService.uploadImage(input.files[0], p => this.progress = p);
      this.book.imageUrl = result;
    } catch (error) {
      if (error instanceof HttpErrorResponse && error.status === 400) {
        const err = error.error as ValidationError;
        err.modelState.imageUrl = err.modelState.file;
        this.errorEmitter.emit(err);
      } else {
        throw error;
      }
    }
    this.progress = 0;
  }

}
