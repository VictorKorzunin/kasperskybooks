import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { deepClone } from '../../helpers/util';
import { Book } from '../../models/book';
import { BookEditComponent } from './book-edit.component';

@Component({
  selector: 'app-book-edit-dialog',
  templateUrl: './book-edit-dialog.component.html',
})
export class BookEditDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) data: Book,
    private dialogRef: MatDialogRef<BookEditDialogComponent>
  ) {
    this.editMode = !!data;
    this.data = deepClone(data) || new Book();
  }

  public editMode = false;

  public data: Book;

  @ViewChild(BookEditComponent)
  public bookEdit: BookEditComponent;

  public submit(event: any) {
    this.bookEdit.form.onSubmit(event);
  }

  public onSubmit(event: Book) {
    this.dialogRef.close(event);
  }

}
