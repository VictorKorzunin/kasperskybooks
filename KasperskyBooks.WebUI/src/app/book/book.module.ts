import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { BookEditDialogComponent } from './book-edit/book-edit-dialog.component';
import { BookEditComponent } from './book-edit/book-edit.component';
import { BookItemComponent } from './book-item/book-item.component';
import { BookComponent } from './book.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    BookComponent,
    BookItemComponent,
    BookEditComponent,
    BookEditDialogComponent
  ],
  entryComponents: [
    BookEditDialogComponent
  ],
  exports: [
    BookEditDialogComponent
  ]
})
export class BookModule { }
