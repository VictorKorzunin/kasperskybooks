import { Book } from '../models/book';
import * as core from './core.actions';

export interface CoreState {
  books: Book[];
}

const initialState: CoreState = {
  books: []
};

/**
 * Core reducer.
 * @param state Core state.
 * @param action Core actions.
 */
export function coreReducer(state: CoreState = initialState, action: core.Actions): CoreState {
  switch (action.type) {

    case core.SET_BOOKS: {
      return { ...state, books: action.payload };
    }

    case core.ADD_BOOK: {
      return { ...state, books: [ ...state.books, action.payload ] };
    }

    case core.UPDATE_BOOK: {
      const books = [...state.books];
      const index = books.findIndex(w => w.id === action.payload.id);
      if (index > -1) {
        books[index] = action.payload;
      }
      return { ...state, books: books };
    }

    case core.DELETE_BOOK: {
      return { ...state, books: state.books.filter(w => w.id !== action.payload) };
    }

    default: {
      return state;
    }

  }
}
