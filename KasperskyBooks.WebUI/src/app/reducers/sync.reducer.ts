import { Sorting } from '../models/sorting';
import * as sync from './sync.actions';

export interface SyncState {
  sorting: Sorting;
}

const initialState: SyncState = {
  sorting: new Sorting()
};

/**
 * Sync reducer. It will be stored in local storage.
 * @param state Sync state.
 * @param action Sync actions.
 */
export function syncReducer(state: SyncState = initialState, action: sync.Actions): SyncState {
  switch (action.type) {

    case sync.SET_SORTING: {
      return { ...state, sorting: action.payload };
    }

    default: {
      return state;
    }

  }
}
