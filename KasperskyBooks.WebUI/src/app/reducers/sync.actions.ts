import { Action } from '@ngrx/store';

import { Sorting } from '../models/sorting';

export const SET_SORTING = '[Core] Set Sorting';

export class SetSorting implements Action {
  public readonly type = SET_SORTING;

  constructor(public payload: Sorting) { }
}

export type Actions =
  | SetSorting;
