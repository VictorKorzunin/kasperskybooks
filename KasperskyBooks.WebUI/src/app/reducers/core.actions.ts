import { Action } from '@ngrx/store';

import { Book } from '../models/book';

export const FETCH_BOOKS = '[Core] Fetch Books';
export const SET_BOOKS = '[Core] Set Books';
export const ADD_BOOK = '[Core] Add Book';
export const UPDATE_BOOK = '[Core] Update Book';
export const DELETE_BOOK = '[Core] Delete Book';

export class FetchBooks implements Action {
  public readonly type = FETCH_BOOKS;
}

export class SetBooks implements Action {
  public readonly type = SET_BOOKS;

  constructor(public payload: Book[]) { }
}

export class AddBook implements Action {
  public readonly type = ADD_BOOK;

  constructor(public payload: Book) { }
}

export class UpdateBook implements Action {
  public readonly type = UPDATE_BOOK;

  constructor(public payload: Book) { }
}

export class DeleteBook implements Action {
  public readonly type = DELETE_BOOK;

  constructor(public payload: number) { }
}

export type Actions =
  | FetchBooks
  | SetBooks
  | AddBook
  | UpdateBook
  | DeleteBook;
