import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Rx';

import { BookService } from '../services/book.service';
import { AppState } from './app.reducer';
import * as coreActions from './core.actions';

/**
 * Incapsulates app reducer side effects.
 */
@Injectable()
export class CoreEffects {

  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private bookService: BookService,
  ) {}

  /**
   * Fetch books from the server and store them in application state.
   */
  @Effect()
  public fetchBooks$ = this.actions$.ofType<coreActions.FetchBooks>(coreActions.FETCH_BOOKS)
    .switchMap(action =>
      this.bookService.getAll()
        .switchMap(payload => Observable.of(new coreActions.SetBooks(payload)))
    );

}
