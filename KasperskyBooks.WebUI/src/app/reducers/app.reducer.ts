import { ActionReducer, ActionReducerMap } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';

import { coreReducer, CoreState } from './core.reducer';
import { syncReducer, SyncState } from './sync.reducer';

/**
 * Application state.
 */
export interface AppState {
  core: CoreState;
  sync: SyncState;
}

/**
 * Application state reducers.
 */
export const reducers: ActionReducerMap<AppState> = {
  core: coreReducer,
  sync: syncReducer
};

/**
 * Meta reducer factory. Creates a meta reducer which stores a particular state in the local storage.
 */
export function localStorageSyncMetaReducerFactory(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({ keys: ['sync'], rehydrate: true })(reducer);
}
