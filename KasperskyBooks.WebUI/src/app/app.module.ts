import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppComponent } from './app.component';
import { BookComponent } from './book/book.component';
import { BookModule } from './book/book.module';
import { CoreEffects, reducers } from './reducers';
import { localStorageSyncMetaReducerFactory } from './reducers/app.reducer';
import { BookService } from './services/book.service';
import { DialogService } from './services/dialog.service';
import { SharedModule } from './shared/shared.module';

const routes: Routes = [
  { path: '', component: BookComponent },
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    HttpClientModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot(reducers, { metaReducers: [localStorageSyncMetaReducerFactory] }),
    StoreDevtoolsModule.instrument({ maxAge: 25 }),
    EffectsModule.forRoot([CoreEffects]),
    BookModule,
    SharedModule
  ],
  providers: [
    BookService,
    DialogService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
