import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { BookEditDialogComponent } from './book/book-edit/book-edit-dialog.component';
import { Book } from './models/book';
import { AppState, coreActions } from './reducers';
import { BookService } from './services/book.service';
import { DialogService } from './services/dialog.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private dialogService: DialogService,
    private bookService: BookService,
    private store: Store<AppState>,
  ) {}

  public async addBook() {
    const result = await this.dialogService.showEditDialog<Book>(BookEditDialogComponent, null);
    if (result) {
      this.store.dispatch(new coreActions.AddBook(result));
    }
  }

}
