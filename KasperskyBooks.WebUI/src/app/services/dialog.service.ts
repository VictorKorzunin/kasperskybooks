import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';

import { DialogResult } from '../models/dialog-result';
import { ConfirmDialogComponent } from '../shared/confirm-dialog/confirm-dialog.component';
import { InfoDialogComponent } from '../shared/info-dialog/info-dialog.component';

/**
 * Provides common types of dialogs.
 */
@Injectable()
export class DialogService {

  constructor(
    private dialog: MatDialog
  ) { }

  /**
   * Presents info dialog.
   * @param message Dialog message.
   * @param title Dialog title.
   */
  public info(message: string, title: string = 'Info'): Promise<DialogResult> {
    return this.presentInfoDialog(message, title);
  }

  /**
   * Presents confirmation dialog.
   * @param {string} message Confirmation message.
   * @param {string} title Optional dialog title.
   */
  public confirm(message: string, title = 'Confirm'): Promise<DialogResult> {
    return this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: title,
        message: message
      },
      disableClose: true,
      panelClass: 'dialog-medium'
    }).afterClosed().toPromise<DialogResult>();
  }

  /**
   * Presents error dialog.
   * @param message Dialog message.
   * @param title Dialog title.
   */
  public error(message: string, title = 'Error'): Promise<DialogResult> {
    return this.presentInfoDialog(message, title);
  }

  /**
   * Presents fatal error dialog.
   * @param message Dialog message.
   * @param title Dialog title.
   */
  public fatalError(message: string, title = 'Error'): Promise<DialogResult> {
    return this.presentInfoDialog(message, title, true);
  }

  private presentInfoDialog(message: string, title: string, showClose = true): Promise<DialogResult> {
    return this.dialog.open(InfoDialogComponent, {
      data: {
        title,
        message,
        showClose
      },
      panelClass: 'dialog-medium'
    }).afterClosed().toPromise<DialogResult>();
  }

  /**
   * Present edit dialog by given component.
   * @param componentOrTemplateRef Given component.
   * @param model Model instance for editing.
   */
  public async showEditDialog<T>(componentOrTemplateRef: any, model?: T): Promise<T> {
    const dialogRef = this.dialog.open(componentOrTemplateRef, {
      data: model,
      panelClass: 'edit-dialog',
      width: '450px'
    });
    return dialogRef.afterClosed().toPromise<T>();
  }

}
