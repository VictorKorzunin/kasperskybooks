import 'rxjs/add/operator/map';

import { HttpClient, HttpEventType, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../environments/environment';
import { Book } from '../models/book';


/**
 * Book http service. Communicates with the server.
 */
@Injectable()
export class BookService {

  constructor(private http: HttpClient) { }

  /**
   * Get all boosk.
   */
  public getAll(): Observable<Book[]> {
    return this.http.get<Book[]>(`${environment.baseApiUrl}/books`);
  }

  /**
   * Create a new book.
   * @param book Book model.
   */
  public create(book: Book): Promise<number> {
    return this.http.post<number>(`${environment.baseApiUrl}/books`, book).toPromise();
  }

  /**
   * Update a book.
   * @param book Book model.
   */
  public update(book: Book): Promise<void> {
    return this.http.put<void>(`${environment.baseApiUrl}/books/${book.id}`, book).toPromise();
  }

  /**
   * Delete a book by id.
   * @param id Book identifier.
   */
  public delete(id: number): Promise<void> {
    return this.http.delete<void>(`${environment.baseApiUrl}/books/${id}`).toPromise();
  }

  /**
   * Upload image to the server an return direct url.
   * @param file Image file.
   * @param progressCallback Uploading progress callback.
   */
  public uploadImage(file: Blob, progressCallback: (progress: number) => void): Promise<string> {
    const formData = new FormData();
    formData.append('file', file);
    const req = new HttpRequest('POST', `${environment.baseApiUrl}/books/upload-image`, formData, {
      reportProgress: true
    });
    return new Promise((resolve, reject) => {
      this.http.request<string>(req)
        .catch(err => {
          reject(err);
          return Observable.throw(err);
        })
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            // tslint:disable-next-line:no-magic-numbers
            const percentDone = Math.round(100 * event.loaded / event.total);
            progressCallback(percentDone);
          } else if (event instanceof HttpResponse) {
            resolve(event.body);
          }
        });
    });
  }

}
