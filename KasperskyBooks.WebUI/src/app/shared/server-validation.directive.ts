import { Directive, EventEmitter, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ValidationError } from '../models/validation-error';

/**
 * Server validation directive.
 */
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[server-validation]form'
})
export class ServerValidationDirective implements OnInit {

  /**
   *
   * @param ngForm Injected angular form.
   */
  constructor(private ngForm: NgForm) { }

  /**
   * Validation error emitter passed from page.
   */
  // tslint:disable-next-line:no-input-rename
  @Input('server-validation')
  public errorEmitter: EventEmitter<ValidationError>;

  /**
   * @inheritdoc
   */
  public ngOnInit() {
    this.errorEmitter.subscribe((validationError: ValidationError) => {
      if (validationError.modelState) {
        for (const key in validationError.modelState) {
          if (validationError.modelState.hasOwnProperty(key)) {
            const normalizedKey = this.normalizeKey(key);
            const control = this.ngForm.controls[normalizedKey];
            if (control) {
              control.setErrors(validationError.modelState[key]);
            }
          }
        }
      }
    });
  }

  private normalizeKey(key: string): string {
    const replacedKey = key.replace('value.', '');
    return replacedKey.charAt(0).toLowerCase() + replacedKey.slice(1);
  }

}
