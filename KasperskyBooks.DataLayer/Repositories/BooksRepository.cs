﻿using System.Collections.Generic;
using System.Linq;
using KasperskyBooks.DataLayer.Models;

namespace KasperskyBooks.DataLayer.Repositories
{
    public class BooksRepository : IRepository<Book>
    {
        private readonly List<Book> _books = new List<Book>()
        {
            new Book()
            {
                Id = 1,
                Title = "CLR via C#",
                Authors = new []
                {
                    new Author() { FirstName = "Джеффри", LastName = "Рихтер" }
                },
                PageCount = 896,
                Issuer = "Питер",
                IssueYear = 2017,
                Isbn = "978-5-496-00433-6",
                ImageUrl = "http://ozon-st.cdn.ngenix.net/multimedia/1007028713.jpg"
            },
            new Book()
            {
                Id = 2,
                Title = "Совершенный код",
                Authors = new []
                {
                    new Author() { FirstName = "Стив", LastName = "Макконнелл" }
                },
                PageCount = 896,
                Issuer = "Русская Редакция, Microsoft Press",
                IssueYear = 2017,
                Isbn = "978-5-7502-0064-1",
                ImageUrl = "https://ozon-st.cdn.ngenix.net/multimedia/1016286803.jpg"
            },
            new Book()
            {
                Id = 3,
                Title = "Приемы объектно-ориентированного проектирования",
                Authors = new []
                {
                    new Author() { FirstName = "Эрих", LastName = "Гамма" },
                    new Author() { FirstName = "Ричард", LastName = "Хелм" },
                    new Author() { FirstName = "Ральф", LastName = "Джонсон" },
                    new Author() { FirstName = "Джон", LastName = "Влиссидес" }
                },
                PageCount = 366,
                Issuer = "Питер",
                IssueYear = 2016,
                Isbn = "978-5-459-01720-5",
                ImageUrl = "https://ozon-st.cdn.ngenix.net/multimedia/1000281214.jpg"
            },
        };

        public Book Get(int id)
        {
            return _books.FirstOrDefault(w => w.Id == id);
        }

        public IEnumerable<Book> GetAll()
        {
            return _books;
        }

        public int? Insert(Book entity)
        {
            entity.Id = _books.Max(w => w.Id) + 1;
            _books.Add(entity);
            return entity.Id;
        }

        public void Update(int id, Book entity)
        {
            var book = _books.FirstOrDefault(w => w.Id == id);
            if (book != null)
            {
                book.Title = entity.Title;
                book.Authors = entity.Authors;
                book.PageCount = entity.PageCount;
                book.Issuer = entity.Issuer;
                book.IssueYear = entity.IssueYear;
                book.Isbn = entity.Isbn;
                book.ImageUrl = entity.ImageUrl;
            }
        }

        public void Delete(int id)
        {
            var book = _books.FirstOrDefault(w => w.Id == id);
            if (book != null)
            {
                _books.Remove(book);
            }
        }
    }
}
