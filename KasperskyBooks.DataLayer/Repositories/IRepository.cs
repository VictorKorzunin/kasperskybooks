﻿using System.Collections.Generic;

namespace KasperskyBooks.DataLayer.Repositories
{
    /// <summary>
    /// Repository abstraction for basic CRUD operations.
    /// </summary>
    /// <typeparam name="TEntity">The entity repository wraps.</typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Gets entity instance by id.
        /// </summary>
        /// <param name="id">Entity id.</param>
        /// <returns>Entity instance.</returns>
        TEntity Get(int id);

        /// <summary>
        /// Gets all entities of the specified type.
        /// </summary>
        /// <returns>Enumerable of entities.</returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Inserts the entity into the database.
        /// </summary>
        /// <param name="entity">Entity instance.</param>
        /// <returns>The ID (primary key) of the newly inserted record if it is identity using the int? type, otherwise null.</returns>
        int? Insert(TEntity entity);

        /// <summary>
        /// Updates the entity in the database.
        /// </summary>
        /// <param name="id">Entity id.</param>
        /// <param name="entity">Entity instance.</param>
        void Update(int id, TEntity entity);

        /// <summary>
        /// Deletes the entity in the database that match the passed object.
        /// </summary>
        /// <param name="id">Entity id.</param>
        void Delete(int id);
    }
}