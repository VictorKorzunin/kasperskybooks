﻿namespace KasperskyBooks.DataLayer.Models
{
    public class Book
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public Author[] Authors { get; set; }

        public int PageCount { get; set; }

        public string Issuer { get; set; }

        public int IssueYear { get; set; }

        public string Isbn { get; set; }

        public string ImageUrl { get; set; }
    }
}
