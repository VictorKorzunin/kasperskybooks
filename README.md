# Kaspersky Books #

Test task.

API documentation: http://localhost:53525/Help

### How do I get set up? ###

1. Open KasperskyBooks solution and run KasperskyBooks.WebAPI project.
2. Open KasperskyBooks.WebUI folder
3. In command line: 

Install node_modules packages:
```
npm install
```
Run the app:
```
npm start
```

You should have NodeJS and NPM installed.

### Who do I talk to? ###

* Victor Korzunin
* ifloydrose@gmail.com