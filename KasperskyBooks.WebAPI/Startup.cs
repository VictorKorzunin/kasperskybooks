﻿using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Newtonsoft.Json.Serialization;
using Owin;

[assembly: OwinStartup(typeof(KasperskyBooks.Startup))]

namespace KasperskyBooks
{
    /// <summary>
    /// OWIN automatic startup class.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// HttpConfiguration instance. Required for HelpPage area.
        /// </summary>
        public static HttpConfiguration HttpConfiguration { get; private set; }

        /// <summary>
        /// Is being run by OWIN pipeline.
        /// </summary>
        /// <param name="app">OWIN app builder.</param>
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration = new HttpConfiguration();

            AreaRegistration.RegisterAllAreas();
            RouteConfig.Register(HttpConfiguration);
            AutoMapperConfig.Register();
            app.RegisterAutofacConfig(HttpConfiguration);

            // Set CamelCase JSON format
            HttpConfiguration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            HttpConfiguration.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(HttpConfiguration);
        }
    }
}
