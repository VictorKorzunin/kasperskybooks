﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KasperskyBooks.ViewModels
{
    /// <summary>
    /// Author model.
    /// </summary>
    public class AuthorViewModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
    }
}