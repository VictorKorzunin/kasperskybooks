﻿using DataAnnotationsExtensions;
using System.ComponentModel.DataAnnotations;
using KasperskyBooks.DataAnnotations;

namespace KasperskyBooks.ViewModels
{
    /// <summary>
    /// Book model.
    /// </summary>
    public class BookViewModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Title { get; set; }

        [Required]
        [EnumerableMinCount(1)]
        public AuthorViewModel[] Authors { get; set; }

        [Required]
        [Min(1)]
        [Max(10000)]
        public int PageCount { get; set; }

        [MaxLength(30)]
        public string Issuer { get; set; }

        [Min(1800)]
        public int IssueYear { get; set; }

        [RegularExpression(@"^(?:ISBN(?:-13)?:?\ )?(?=[0-9]{13}$|(?=(?:[0-9]+[-\ ]){4})[-\ 0-9]{17}$)97[89][-\ ]?[0-9]{1,5}[-\ ]?[0-9]+[-\ ]?[0-9]+[-\ ]?[0-9]$")]
        public string Isbn { get; set; }

        public string ImageUrl { get; set; }
    }
}