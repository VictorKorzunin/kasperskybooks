﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;

namespace KasperskyBooks.DataAnnotations
{
    /// <summary>
    /// Custom attribute annotates an IEnumerable propery with minimum valid count of elements.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class EnumerableMinCountAttribute : ValidationAttribute
    {
        private readonly int _min;

        public object Min => (object)this._min;

        public EnumerableMinCountAttribute(int min) : base("min")
        {
            this._min = min;
        }

        /// <inheritdoc/>
        public override string FormatErrorMessage(string name)
        {
            if (this.ErrorMessage == null && this.ErrorMessageResourceName == null)
                this.ErrorMessage = "Enumerable {0} should have atleast {1} valid items.";
            return string.Format((IFormatProvider)CultureInfo.CurrentCulture, this.ErrorMessageString, new object[2]
            {
                (object) name,
                (object) this._min
            });
        }

        /// <inheritdoc/>
        public override bool IsValid(object value)
        {
            if (value == null)
                return true;

            var result = value as IEnumerable<object>;

            return result?.Count() >= _min;
        }
    }
}