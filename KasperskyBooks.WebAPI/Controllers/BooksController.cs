﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using AutoMapper;
using KasperskyBooks.DataLayer.Models;
using KasperskyBooks.DataLayer.Repositories;
using KasperskyBooks.ViewModels;

namespace KasperskyBooks.Controllers
{
    /// <summary>
    /// Books controller.
    /// </summary>
    [RoutePrefix("api/books")]
    public class BooksController : ApiController
    {
        private readonly IRepository<Book> _repository;

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="repository">Books repository abstraction.</param>
        public BooksController(IRepository<Book> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get all books.
        /// </summary>
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(Mapper.Map<IEnumerable<BookViewModel>>(_repository.GetAll()));
        }

        /// <summary>
        /// Get book by id.
        /// </summary>
        /// <param name="id">Book identifier.</param>
        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            return Ok(Mapper.Map<BookViewModel>(_repository.Get(id)));
        }

        /// <summary>
        /// Create a new book.
        /// </summary>
        /// <param name="value">Book model.</param>
        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromBody]BookViewModel value)
        {
            if (ModelState.IsValid)
            {
                var id = _repository.Insert(Mapper.Map<Book>(value));
                return Ok(id);
            }
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Update a book by id.
        /// </summary>
        /// <param name="id">Book identifier.</param>
        /// <param name="value">Book model.</param>
        [HttpPut]
        [Route("{id}")]
        public IHttpActionResult Put(int id, [FromBody]BookViewModel value)
        {
            if (ModelState.IsValid)
            {
                _repository.Update(id, Mapper.Map<Book>(value));
                return Ok("Recored has been updated!");
            }
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Delete book by id.
        /// </summary>
        /// <param name="id">Book identifier.</param>
        [HttpDelete]
        [Route("{id}")]
        public IHttpActionResult Delete(int id)
        {
            _repository.Delete(id);
            return Ok("Record has been deleted!");
        }

        /// <summary>
        /// Upload an image, save it in storage and return public url.
        /// </summary>
        [HttpPost]
        [Route("upload-image")]
        public IHttpActionResult UploadImage()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        const int maxContentLength = 1024 * 1024 * 2; // Size = 2 MB  

                        IList<string> allowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!allowedFileExtensions.Contains(extension))
                        {
                            ModelState.AddModelError("file", "Please Upload image of type .jpg,.gif,.png.");
                            return BadRequest(ModelState);
                        }
                        if (postedFile.ContentLength > maxContentLength)
                        {
                            ModelState.AddModelError("file", "Please Upload a file up to 2 mb.");
                            return BadRequest(ModelState);
                        }
                        var filePath = HttpContext.Current.Server.MapPath("~/Assets/" + postedFile.FileName + extension);

                        postedFile.SaveAs(filePath);

                        return Ok(Request.RequestUri.GetLeftPart(UriPartial.Authority) + "/Assets/" + postedFile.FileName + extension);
                    }
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
