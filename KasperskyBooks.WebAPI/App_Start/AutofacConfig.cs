﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using KasperskyBooks.DataLayer.Models;
using KasperskyBooks.DataLayer.Repositories;
using Owin;

namespace KasperskyBooks
{
    public static class AutofacConfig
    {
        public static void RegisterAutofacConfig(this IAppBuilder app, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            
            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<BooksRepository>().As<IRepository<Book>>().SingleInstance();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
        }
    }
}