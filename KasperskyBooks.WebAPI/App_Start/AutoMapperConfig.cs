﻿using AutoMapper;

namespace KasperskyBooks
{
    /// <summary>
    /// AutoMapper configuration class.
    /// </summary>
    public static class AutoMapperConfig
    {
        /// <summary>
        /// Registers AutoMapper config.
        /// </summary>
        public static void Register()
        {
            // Scan for all profiles in the assembly
            Mapper.Initialize(cfg => cfg.AddProfiles(typeof(AutoMapperConfig).Assembly));
        }
    }
}