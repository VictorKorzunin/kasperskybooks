﻿using System.Web.Http;

namespace KasperskyBooks
{
    /// <summary>
    /// WebApi Route config.
    /// </summary>
    public static class RouteConfig
    {
        /// <summary>
        /// Registers WebApi routes.
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            // WebApi routes
            config.MapHttpAttributeRoutes();
        }
    }
}
