﻿using AutoMapper;
using KasperskyBooks.DataLayer.Models;
using KasperskyBooks.ViewModels;

namespace KasperskyBooks.Mappings
{
    /// <summary>
    /// Mapping profile for <see cref="BookProfile"/> and related models.
    /// </summary>
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, BookViewModel>().ReverseMap();
            CreateMap<Author, AuthorViewModel>().ReverseMap();
        }
    }
}